from typing import Any, Optional
from datetime import datetime, timedelta
from pathlib import Path

from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import rsa

def create_key() -> Any:
	return rsa.generate_private_key(
		public_exponent = 65537, key_size = 2048,
		backend = default_backend()
	)

def create_csr(key: Any, *, common_name: Optional[str] = None, domain: Optional[str] = None) -> Any:
	if common_name is None:
		common_name = domain
	
	if common_name is None:
		raise ValueError("either `common_name` or `domain` required")
	
	csr = x509.CertificateSigningRequestBuilder()
	csr = csr.subject_name(x509.Name([x509.NameAttribute(NameOID.COMMON_NAME, common_name)]))
	if domain:
		csr = csr.add_extension(x509.SubjectAlternativeName([x509.DNSName(domain)]), critical = False)
	csr = csr.sign(key, hashes.SHA256(), default_backend())
	return csr

def sign_csr(csr: Any, issuer: str, issuer_key: Any, *, days: Optional[int] = None) -> Any:
	if days is None:
		days = 30
	
	cert = x509.CertificateBuilder()
	cert = cert.subject_name(csr.subject)
	for ext in csr.extensions:
		cert = cert.add_extension(ext.value, critical = ext.critical)
	cert = cert.issuer_name(issuer)
	cert = cert.public_key(csr.public_key())
	cert = cert.serial_number(x509.random_serial_number())
	now = datetime.utcnow()
	cert = cert.not_valid_before(now)
	cert = cert.not_valid_after(now + timedelta(days = days))
	cert = cert.sign(issuer_key, hashes.SHA256(), default_backend())
	return cert

def load_key(path: Path) -> Any:
	backend = default_backend()
	with path.open('rb') as fh:
		return serialization.load_pem_private_key(fh.read(), None, backend)

def save_key(key: Any, path: Path) -> None:
	with path.open('wb') as fh:
		fh.write(key.private_bytes(
			encoding = serialization.Encoding.PEM,
			format = serialization.PrivateFormat.TraditionalOpenSSL,
			encryption_algorithm = serialization.NoEncryption(),
		))

def load_cert(path: Path) -> Any:
	backend = default_backend()
	with path.open('rb') as fh:
		return x509.load_pem_x509_certificate(fh.read(), backend)

def save_cert(crt: Any, path: Path) -> None:
	with path.open('wb') as fh:
		fh.write(crt.public_bytes(serialization.Encoding.PEM))
