from typing import List, Any
import os
import importlib

def get_stores() -> List[Any]:
	stores = [] # type: List[Any]
	os_store = importlib.import_module('.store_os_{}'.format(os.name), 'devtls')
	stores.append(os_store)
	return stores
