from typing import Dict, Any, Tuple, Iterable, Optional, Union
from datetime import datetime, timedelta
from pathlib import Path
import shutil
import ssl

from . import crypto_util, store

class DevTLS:
	def __init__(self, app_name: str, *, cert_dir: Union[str, Path, None] = None) -> None:
		self.cert_dir = Path(cert_dir or '.devtls_cache')
		self.cert_root = 'DO_NOT_TRUST_DevTLS_{}'.format(app_name)
		self._cert_cache = {} # type: Dict[str, ssl.SSLContext]
		self.stores = store.get_stores()
	
	def create_ssl_context(self) -> ssl.SSLContext:
		# Immediately create+install root cert
		self._get_or_create_root_cert()
		
		ssl_context = ssl.create_default_context(purpose = ssl.Purpose.CLIENT_AUTH)
		
		cache = self._cert_cache
		def servername_callback(socket: Any, domain: Optional[str], ssl_context: Any) -> None:
			# TODO: What do?
			assert domain is not None
			
			if domain not in cache:
				ctxt = ssl.create_default_context(purpose = ssl.Purpose.CLIENT_AUTH)
				p_crt, p_key = self._get_or_create_cert(domain)
				ctxt.load_cert_chain(str(p_crt), keyfile = str(p_key))
				cache[domain] = ctxt
			socket.context = cache[domain]
		
		ssl_context.set_servername_callback(servername_callback)
		return ssl_context
	
	def remove_cert_dir(self) -> None:
		try: shutil.rmtree(str(self.cert_dir), ignore_errors = True)
		except: pass
	
	def _get_or_create_cert(self, domain: str) -> Tuple[Path, Path]:
		p_crt = self.cert_dir / '{}.crt'.format(domain)
		p_key = self.cert_dir / '{}.key'.format(domain)
		
		if not exists_and_valid(p_crt, p_key):
			crt, key = self._create_cert(domain)
			self.cert_dir.mkdir(parents = True, exist_ok = True)
			crypto_util.save_key(key, p_key)
			crypto_util.save_cert(crt, p_crt)
		
		return p_crt, p_key
	
	def _create_cert(self, domain: str) -> Tuple[Any, Any]:
		key = crypto_util.create_key()
		csr = crypto_util.create_csr(key, domain = domain)
		if self.cert_root:
			p_crt, p_key = self._get_or_create_root_cert()
			signer_cert = crypto_util.load_cert(p_crt)
			signer_subject = signer_cert.subject
			signer_key = crypto_util.load_key(p_key)
			days = (signer_cert.not_valid_after - datetime.utcnow()).days
		else:
			signer_subject = csr.subject
			signer_key = key
			days = None
		crt = crypto_util.sign_csr(csr, signer_subject, signer_key, days = days)
		return crt, key
	
	def _get_or_create_root_cert(self) -> Tuple[Path, Path]:
		assert self.cert_root is not None
		
		p_crt = self.cert_dir / '{}.crt'.format(self.cert_root)
		p_key = self.cert_dir / '{}.key'.format(self.cert_root)
		
		if not exists_and_valid(p_crt, p_key):
			self._cert_cache.clear()
			self.remove_cert_dir()
			
			crt, key = self._create_root_cert()
			self.cert_dir.mkdir(parents = True, exist_ok = True)
			crypto_util.save_key(key, p_key)
			crypto_util.save_cert(crt, p_crt)
			
			for store in self.stores:
				store.remove_old_and_install_new(self.cert_root, p_crt)
		
		return p_crt, p_key
	
	def _create_root_cert(self) -> Tuple[Any, Any]:
		key = crypto_util.create_key()
		csr = crypto_util.create_csr(key, common_name = self.cert_root)
		crt = crypto_util.sign_csr(csr, csr.subject, key)
		return crt, key

def exists_and_valid(p_crt: Path, p_key: Path) -> bool:
	if not p_crt.exists(): return False
	if not p_key.exists(): return False
	crt = crypto_util.load_cert(p_crt)
	now = datetime.utcnow()
	if now < crt.not_valid_before: return False
	near_future = now + timedelta(days = 1)
	if near_future > crt.not_valid_after: return False
	return True
