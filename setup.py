import setuptools

with open('README.md', 'r') as fh:
	long_description = fh.read()

setuptools.setup(
	name = 'devtls',
	version = '0.2.0',
	author = "valtron",
	description = "On-demand generation of TLS certificates signed by an autogenerated trusted root.",
	long_description = long_description,
	long_description_content_type = 'text/markdown',
	url = 'https://gitlab.com/valtron/devtls',
	packages = ['devtls'],
	python_requires = '>=3.5',
	install_requires = ['cryptography', 'aiohttp'],
	classifiers = [
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.5',
		'Programming Language :: Python :: 3.6',
		'Programming Language :: Python :: 3.7',
		'Operating System :: OS Independent',
		'License :: OSI Approved :: MIT License',
	],
)
